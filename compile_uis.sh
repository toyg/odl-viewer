#!/usr/bin/env bash
source env/bin/activate
for f in ./qt/ODLViewer/*.ui; do
    name=`basename $f`
    no_ui_name=${name%.*}
    pyuic5 -o ./ODLViewer/ui/$no_ui_name.py -x $f
done