import os
from os.path import exists
import sys
from PyQt5.QtCore import QFileInfo, QSize, QSettings
from PyQt5.QtGui import QGuiApplication, QIcon
from PyQt5.QtWidgets import QApplication

def _get_app_version():
    # todo: read from txt or windows registry etc
    return "1.0"


class ODLViewer(QApplication):
    """ Main application instance
    """
    def __init__(self, args):
        super().__init__(args)
        self.setApplicationName('ODLViewer')
        self.setApplicationDisplayName("ODL Viewer")
        self.setApplicationVersion(_get_app_version())
        self.setOrganizationName('Infratects')
        self.setOrganizationDomain('infratects.com')
        QSettings().setValue('email_contact', 'glacava@infratects.com')
        # preload all icons, so they can be accessed
        # with QApplication.instance().icons['icon-name']
        self.icons = {}

        # PyInstaller puts data files in a var called sys._MEIPASS.
        # However, in some cases (e.g. development) this is not set;
        # so we first set the root to a "nromal" location, then we check if
        # we're running as pyinstaller-packed bundle and if yes, change it to
        # the special dir.

        _root = QFileInfo(__file__).absolutePath()
        try:
            _root = sys._MEIPASS
        except AttributeError as e:
            pass
        # preload all icons
        for icon_name in ['open', 'open_folder', 'error',
                          'warning', 'info', 'trace', 'filter',
                          'find', 'search_folder', 'about']:
            icon = QIcon()
            for size in (16, 24, 32, 64, 128):
                # qt doesn't care about path-separators, so we can freely use /
                file_path = _root + '/icons/{size}x{size}/{name}.png'.format(
                    size=size,
                    name=icon_name)
                if exists(file_path):
                    icon.addFile(file_path, QSize(size, size))
            self.icons[icon_name] = icon
        self.main_window = OVMainWindow()




if __name__ == '__main__':
    qApp = QGuiApplication(sys.argv)
    # import has to be here because QGuiApplication() has to be loaded first
    from ovmain import OVMainWindow
    app = ODLViewer(sys.argv)
    sys.exit(app.exec_())
