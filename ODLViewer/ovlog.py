from urllib.parse import quote_plus

from PyQt5.QtCore import Qt, QRegExp, QDateTime, QCoreApplication, QUrl, QSettings, QModelIndex

from PyQt5.QtGui import QCursor, QDesktopServices, QColor, QBrush
from PyQt5.QtWidgets import QAbstractItemView, QDataWidgetMapper, QMainWindow, \
    QDialog, QLabel, QLineEdit, QFormLayout, QApplication, QAction, QDialogButtonBox, QMenu, QStyledItemDelegate

from libodl import LogMessage
from models import PropertyModel, LogstackModel, LogfileModelProxy
from ui.filter import Ui_filterDialog
from ui.logmain import Ui_LogMain


# There are two types of windows:
# OVLogWindow contains the list of messages etc.
# OVLogFilter is the modal filter dialog.

# for file-bound subclasses, see ovlogfile.py and ovlogfiles.py

_translate = QCoreApplication.translate

class RowDelegate(QStyledItemDelegate):

    def _get_color_for_msglevel(self, msglevel_string):
        map = {'ERROR': QColor(236,163,163),
               'WARNING': QColor(233,195,135),
               'WARN': QColor(233,195,135),
               'NOTIFICATION': QColor(255,255,255),
               'INFO': QColor(255,255,255),
               'DEBUG': QColor(211,208,205),
               'TRACE': QColor(211,208,205),
               }
        for key in map:
            if msglevel_string.startswith(key):
                return QBrush(map[key])

    def initStyleOption(self, optionviewitem, index):
        super().initStyleOption(optionviewitem, index)
        model = index.model()
        msg_type = model.data(
            model.createIndex(index.row(), model.fields.index('type')),
            Qt.DisplayRole)
        optionviewitem.backgroundBrush = self._get_color_for_msglevel(msg_type)

class OVLogFilter(QDialog, Ui_filterDialog):
    """ Modal with filtering options.
    Some fields are fixed (date range and ODL required fields),
    the rest are dynamically derived from the main model.
    """
    def __init__(self, parentLogWindow):
        super().__init__(parentLogWindow)
        self.setupUi(self)
        self.setModal(True)
        model = parentLogWindow.tMessages.model().sourceModel()
        # get mindate and maxdate from source data
        self.set_default_date_range()
        self.dtFrom.setCalendarWidget(self.calFrom)
        self.dtTo.setCalendarWidget(self.calTo)
        # set up ODL required fields
        self.filterWidgets = {'component_id': self.leCompID,
                              'ecid': self.leECID,
                              'module_id': self.leModID,
                              'id': self.leMsgID}
        # set up extra fields
        self.extra_fields = [field for field in model.fields if field not in LogMessage.REQUIRED_FIELDS]
        self.extraLayout = QFormLayout(self.grpExtra)
        self.extraLayout.setContentsMargins(0, 0, 0, 0)
        self.extraLayout.setSpacing(1)
        self.extraLayout.setObjectName("extraLayout")
        # create label and editor for each extra field
        for rownum, field in enumerate(self.extra_fields):
            fLabel = QLabel(self.grpExtra)
            fEdit = QLineEdit(self.grpExtra)
            fLabel.setText(field.capitalize())
            fEdit.setObjectName('filter_' + field)
            self.extraLayout.setWidget(rownum, QFormLayout.LabelRole, fLabel)
            self.extraLayout.setWidget(rownum, QFormLayout.FieldRole, fEdit)
            self.filterWidgets[field] = fEdit
        # connect reset action
        self.btnFilter.button(QDialogButtonBox.Reset).clicked.connect(self.reset_filter)

    def reset_filter(self, *args, **kwargs):
        """ Clear all filters
        """
        for key, widget in self.filterWidgets.items():
            widget.clear()
        self.set_default_date_range()

    def set_default_date_range(self):
        """ find min and max values in source data
        and init the widgets to them.
        """
        # get mindate and maxdate from source data
        model = self.parent().tMessages.model().sourceModel()
        all_dates = set([msg.timestamp for msg in model.messages])
        self.minDate = QDateTime(min(all_dates))
        self.maxDate = QDateTime(max(all_dates))
        # set widgets to specified range
        self.calFrom.setSelectedDate(self.minDate.date())
        self.calTo.setSelectedDate(self.maxDate.date())
        self.dtFrom.setDateTime(self.minDate)
        self.dtTo.setDateTime(self.maxDate)


class WebSearchAction(QAction):
    """ Action to launch a web search from a widget or model index
    """
    def __init__(self, widget, parent, *args, **kwargs):
        super().__init__(parent, *args)
        self.senderWidget = widget
        self.setText(_translate("LogMain", "Web Search"))
        self.setToolTip(_translate("LogMain", "Search the web for selected text"))
        self.triggered.connect(self.search)

    def search(self, *args):
        """ Launch a web search
        """
        text = ""
        # determine which widget requested the search, and retrieve text to search for
        widget = self.senderWidget
        if type(widget) == QModelIndex:
            text = widget.model().data(widget, Qt.DisplayRole)
        elif widget.objectName() == 'txtError':
            text = widget.textCursor().selectedText()
        else:
            text = widget.selectedText()
        # get search template from settings, so we can customize it in theory
        settings = QSettings()
        url_template = settings.value("websearch_template", "https://www.google.com/search?q={}")
        # launch search
        QDesktopServices.openUrl(QUrl(url_template.format(quote_plus(text))))


class FilterAction(QAction):
    """ Action to enable filtering on a given field/value.
    Originally built for tMessages table view.
    """
    def __init__(self, filter_field, filter_value, ovlogwindow, parent, *args):
        super().__init__(parent)
        self.ovl = ovlogwindow,
        self.field = filter_field
        self.value = filter_value
        self.triggered.connect(self.set_filter)
        self.setText(_translate("LogMain", "Filter by this"))
        self.setToolTip(_translate("LogMain", "Filter view by this value"))

    def set_filter(self):
        """ Enable the filter
        """
        # for some reason, self.ovl by now is a tuple of one element. Weird.
        self.ovl[0].set_filter_field(self.field, self.value)


class OVLogWindow(QMainWindow, Ui_LogMain):
    """ Base viewer window.
    """

    def __init__(self, messages=None, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.subWindow = None
        self.msg_model = None
        self.tmsg_model = None
        self.filterDialog = None
        self.mapper = None

        # set display properties
        self.splitterHoriz.setSizes((300,250))
        self.tMessages.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.actionSelectERROR.setIcon(QApplication.instance().icons['error'])
        self.actionSelectTRACE.setIcon(QApplication.instance().icons['trace'])
        self.actionSelectWARNING.setIcon(QApplication.instance().icons['warning'])
        self.actionSelectNOTIFICATION.setIcon(QApplication.instance().icons['info'])
        self.actionFilter.setIcon(QApplication.instance().icons['filter'])

        # create custom menus
        # we basically keep a 'register' of actionable widgets and associated menus, so that
        # we can retrieve the launching widget when an action is selected.
        # This spares us from subclassing each widget
        self.context_menus = {}
        self.selectable_widgets = [
            self.lvType,
            self.lvComponent,
            self.lvModule,
            self.lvMsgID,
            self.txtError,
            self.lvECID
        ]
        for widget in self.selectable_widgets:
            # retrieve the default menu (we don't want to lose standard copypaste etc)
            context_menu = widget.createStandardContextMenu()
            first_default_item = context_menu.actions()[0]
            # create an action and tie it to the widget, so we always know the original sender
            google_action = WebSearchAction(widget, self)
            context_menu.insertAction(first_default_item, google_action)
            # add custom actions and save the menu in our 'registry'
            object_name = widget.objectName()
            if object_name == 'lvECID':
                context_menu.insertAction(first_default_item, self.actionFilter_by_this_ECID)
            context_menu.insertSeparator(first_default_item)
            self.context_menus[object_name] = context_menu

        # set the toolbar on the left
        self.addToolBar(Qt.LeftToolBarArea, self.toolBar)

        # set the custom delegate
       # self.tMessages.setItemDelegate(RowDelegate(self))

        #load data
        if messages:
            self.load_messages(messages)

    def load_messages(self, messages):
        """ Load actual data
        :param messages: list of LogMessage instance
        """
        # set up the main model
        self.msg_model = LogstackModel(messages, self)

        # build a proxy model for table view
        self.tmsg_model = LogfileModelProxy(self)
        self.tmsg_model.setSourceModel(self.msg_model)
        # default filtering is on type (DEBUG etc)
        self.tmsg_model.setFilterKeyColumn(self.msg_model.fields.index('type'))
        self.tMessages.setModel(self.tmsg_model)

        # create filter window
        self.filterDialog = OVLogFilter(self)
        self.filterDialog.hide()

        # map details form
        self.mapper = QDataWidgetMapper(self)
        self.mapper.setModel(self.tmsg_model)  # use the proxy!
        self.mapper.addMapping(self.txtError, self.msg_model.fields.index('text'))
        self.mapper.addMapping(self.lvType, self.msg_model.fields.index('type'))
        self.mapper.addMapping(self.dtevDate, self.msg_model.fields.index('timestamp'))
        self.mapper.addMapping(self.lvECID, self.msg_model.fields.index('ecid'))
        self.mapper.addMapping(self.lvMsgID, self.msg_model.fields.index('id'))
        self.mapper.addMapping(self.lvModule, self.msg_model.fields.index('module_id'))
        self.mapper.addMapping(self.lvComponent, self.msg_model.fields.index('component_id'))

        # override right-click menu
        for widget in self.selectable_widgets:
            widget.setContextMenuPolicy(Qt.CustomContextMenu)
            widget.customContextMenuRequested.connect(self._create_menu)
        self.tMessages.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tMessages.customContextMenuRequested.connect(self._create_tableview_menu)
        self.tExtra.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tExtra.customContextMenuRequested.connect(self._create_tableview_menu)

        # connect signals
        selectionModel = self.tMessages.selectionModel()
        selectionModel.selectionChanged.connect(self._refresh_mappers)
        for action in [self.actionSelectERROR,
                       self.actionSelectWARNING,
                       self.actionSelectNOTIFICATION,
                       self.actionSelectTRACE]:
            action.changed.connect(self.set_level_filter)
        self.filterDialog.accepted.connect(self.apply_user_filter)
        self.actionFilter.triggered.connect(self.filterDialog.show)
        self.actionFilter_by_this_ECID.triggered.connect(self._filter_by_ecid)

        # resize columns we know should be fairly small
        self.tMessages.horizontalHeader().resizeSection(0, 40)  # log line
        self.tMessages.resizeColumnToContents(self.tMessages.model().fields.index('timestamp'))
        self.tMessages.resizeColumnToContents(self.tMessages.model().fields.index('type'))
        self.tMessages.resizeColumnToContents(self.tMessages.model().fields.index('id'))
        self.tMessages.resizeColumnToContents(self.tMessages.model().fields.index('module_id'))
        # go to first row
        self.tMessages.setCurrentIndex(self.tmsg_model.index(0, 0))

    def _create_menu(self, qpoint):
        """ display the context menu associated with sender widget
        """
        sender_name = self.sender().objectName()
        self.context_menus[sender_name].exec(QCursor.pos())

    def _create_tableview_menu(self, qpoint):
        """ Display context menu for tMessages table view, where it makes sense
        """
        # figure out where we are
        qtableview = self.sender()
        model = qtableview.model()
        cell = qtableview.indexAt(qpoint)
        # if it's a column where it makes sense...
        field = model.fields[cell.column()]
        if field not in ('timestamp', 'log_line'):
            # tableview does not have a default menu, create one
            tv_context_menu = QMenu(qtableview)
            # create web-search action
            websearch_action = WebSearchAction(cell, qtableview)
            tv_context_menu.addAction(websearch_action)
            # create a filter action
            # tMessages and tExtra have different models, so be careful
            filter_field = None
            filter_data = None
            if qtableview.objectName() == 'tMessages':
                filter_field = model.fields[cell.column()]
                filter_data = model.data(cell, Qt.DisplayRole)
            elif qtableview.objectName() == 'tExtra':
                # in tExtra the field name is actually a value in the row
                filter_field = model.fields[cell.row()]
                # we always filter by the actual property value, so get that
                filter_data = model.data(model.createIndex(cell.row(), 1), Qt.DisplayRole)
            filter_action = FilterAction(filter_field,
                                         filter_data,
                                         self,
                                         qtableview)
            tv_context_menu.addAction(filter_action)
            # display the menu
            tv_context_menu.popup(qtableview.viewport().mapToGlobal(qpoint))

    def _filter_by_ecid(self, *args, **kwargs):
        """ Shortcut action to set ECID filter
        """
        self.set_filter_field('ecid', self.lvECID.text())

    def _search_web(self, *args, **kwargs):
        """ Launch a web search
        """
        text = ""
        # determine which widget requested the search, and retrieve text to search for
        widget = self.sender().senderWidget
        if widget.objectName() == 'txtError':
            text = widget.textCursor().selectedText()
        else:
            text = widget.selectedText()
        # get search template from settings, so we can customize it in theory
        settings = QSettings()
        url_template = settings.value("websearch_template", "https://www.google.com/search?q={}")
        # launch search
        QDesktopServices.openUrl(QUrl(url_template.format(quote_plus(text))))

    def set_filter_field(self, field, value):
        """ utility function to set a filter on arbitrary LogMessage attributes
        :param field: name of field (LogMessage attribute) to filter on
        :param value: filter value
        """
        for widget_field, widget in self.filterDialog.filterWidgets.items():
            if field == widget_field:
                widget.setText(value)
                self.apply_user_filter()
                break

    def _refresh_mappers(self, qitemselection_selected, qitemselection_deselected):
        """ Refresh mapped widgets
        :param qitemselection_selected: QItemSelection
        :param qitemselection_deselected: QItemSelection
        :return:
        """
        indexes = qitemselection_selected.indexes()
        if len(indexes) > 0:
            # We only care about the first selected row
            index = indexes[0]
            if index.isValid():
                # update form fields
                self.mapper.setCurrentIndex(index.row())
                # retrieve the original message
                # and build a model for the Extra tableview
                original_index = self.tMessages.model().mapToSource(index)
                message = self.msg_model.data(original_index, Qt.UserRole)
                self.tExtra.setModel(PropertyModel(message))

    def set_level_filter(self, *args, **kwargs):
        """ Manage buttons filtering on logging-level
        """
        # determine what is checked
        # -- add here any known synonym for default levels (e.g. DEBUG...)
        state = {'ERROR': self.actionSelectERROR.isChecked(),
                 'WARN': self.actionSelectWARNING.isChecked(),
                 'NOTIFICATION': self.actionSelectNOTIFICATION.isChecked(),
                 'NOTICE': self.actionSelectNOTIFICATION.isChecked(),
                 'INFO': self.actionSelectNOTIFICATION.isChecked(),
                 'TRACE': self.actionSelectTRACE.isChecked(),
                 'DEBUG': self.actionSelectTRACE.isChecked()
        }
        enabled = [k for k, v in state.items() if v is True]
        regexp = '|'.join(enabled)
        self.tmsg_model.setFilterRegExp(QRegExp(regexp, Qt.CaseInsensitive))

    def apply_user_filter(self, *args, **kwargs):
        """ apply optional filters if set
        """
        self.tmsg_model.minDate = self.filterDialog.dtFrom.dateTime()
        self.tmsg_model.maxDate = self.filterDialog.dtTo.dateTime()
        for field, widget in self.filterDialog.filterWidgets.items():
            if widget.text():
                self.tmsg_model.filters[field] = widget.text()
            else:
                self.tmsg_model.filters.pop(field, None)
        self.tmsg_model.invalidateFilter()
        self.tMessages.setCurrentIndex(self.tmsg_model.index(0, 0))


