# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './qt/ODLViewer/folder_find_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dlgFind(object):
    def setupUi(self, dlgFind):
        dlgFind.setObjectName("dlgFind")
        dlgFind.setWindowModality(QtCore.Qt.WindowModal)
        dlgFind.resize(300, 100)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(dlgFind.sizePolicy().hasHeightForWidth())
        dlgFind.setSizePolicy(sizePolicy)
        dlgFind.setMinimumSize(QtCore.QSize(300, 100))
        dlgFind.setModal(True)
        self.formLayout = QtWidgets.QFormLayout(dlgFind)
        self.formLayout.setContentsMargins(10, 10, 10, 10)
        self.formLayout.setSpacing(5)
        self.formLayout.setObjectName("formLayout")
        self.lFolder = QtWidgets.QLabel(dlgFind)
        self.lFolder.setObjectName("lFolder")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.lFolder)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.leFolder = QtWidgets.QLineEdit(dlgFind)
        self.leFolder.setObjectName("leFolder")
        self.horizontalLayout.addWidget(self.leFolder)
        self.tlbFolder = QtWidgets.QToolButton(dlgFind)
        self.tlbFolder.setObjectName("tlbFolder")
        self.horizontalLayout.addWidget(self.tlbFolder)
        self.formLayout.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout)
        self.lFind = QtWidgets.QLabel(dlgFind)
        self.lFind.setObjectName("lFind")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.lFind)
        self.leFind = QtWidgets.QLineEdit(dlgFind)
        self.leFind.setObjectName("leFind")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.leFind)
        self.buttonBox = QtWidgets.QDialogButtonBox(dlgFind)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.SpanningRole, self.buttonBox)

        self.retranslateUi(dlgFind)
        self.buttonBox.accepted.connect(dlgFind.accept)
        self.buttonBox.rejected.connect(dlgFind.reject)
        QtCore.QMetaObject.connectSlotsByName(dlgFind)

    def retranslateUi(self, dlgFind):
        _translate = QtCore.QCoreApplication.translate
        dlgFind.setWindowTitle(_translate("dlgFind", "Find in Folder..."))
        self.lFolder.setText(_translate("dlgFind", "Folder:"))
        self.tlbFolder.setText(_translate("dlgFind", "..."))
        self.lFind.setText(_translate("dlgFind", "Find:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dlgFind = QtWidgets.QDialog()
    ui = Ui_dlgFind()
    ui.setupUi(dlgFind)
    dlgFind.show()
    sys.exit(app.exec_())

