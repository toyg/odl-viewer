# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './qt/ODLViewer/progressframe.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_frameProgress(object):
    def setupUi(self, frameProgress):
        frameProgress.setObjectName("frameProgress")
        frameProgress.resize(298, 54)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(frameProgress.sizePolicy().hasHeightForWidth())
        frameProgress.setSizePolicy(sizePolicy)
        frameProgress.setMinimumSize(QtCore.QSize(0, 40))
        frameProgress.setFrameShape(QtWidgets.QFrame.StyledPanel)
        frameProgress.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.verticalLayout = QtWidgets.QVBoxLayout(frameProgress)
        self.verticalLayout.setContentsMargins(4, 0, 4, 4)
        self.verticalLayout.setSpacing(2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lFilename = QtWidgets.QLabel(frameProgress)
        self.lFilename.setMinimumSize(QtCore.QSize(0, 20))
        self.lFilename.setObjectName("lFilename")
        self.verticalLayout.addWidget(self.lFilename)
        self.lStatus = QtWidgets.QLabel(frameProgress)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lStatus.sizePolicy().hasHeightForWidth())
        self.lStatus.setSizePolicy(sizePolicy)
        self.lStatus.setMinimumSize(QtCore.QSize(0, 22))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.lStatus.setFont(font)
        self.lStatus.setObjectName("lStatus")
        self.verticalLayout.addWidget(self.lStatus)
        self.progress = QtWidgets.QProgressBar(frameProgress)
        self.progress.setMinimumSize(QtCore.QSize(0, 20))
        self.progress.setProperty("value", 0)
        self.progress.setObjectName("progress")
        self.verticalLayout.addWidget(self.progress)

        self.retranslateUi(frameProgress)
        QtCore.QMetaObject.connectSlotsByName(frameProgress)

    def retranslateUi(self, frameProgress):
        _translate = QtCore.QCoreApplication.translate
        frameProgress.setWindowTitle(_translate("frameProgress", "Frame"))
        self.lFilename.setText(_translate("frameProgress", "Filename"))
        self.lStatus.setText(_translate("frameProgress", "Estimating duration..."))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    frameProgress = QtWidgets.QFrame()
    ui = Ui_frameProgress()
    ui.setupUi(frameProgress)
    frameProgress.show()
    sys.exit(app.exec_())

