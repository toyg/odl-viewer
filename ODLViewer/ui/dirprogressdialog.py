# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './qt/ODLViewer/dirprogressdialog.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DirProgressDialog(object):
    def setupUi(self, DirProgressDialog):
        DirProgressDialog.setObjectName("DirProgressDialog")
        DirProgressDialog.setWindowModality(QtCore.Qt.WindowModal)
        DirProgressDialog.resize(377, 393)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(DirProgressDialog.sizePolicy().hasHeightForWidth())
        DirProgressDialog.setSizePolicy(sizePolicy)
        DirProgressDialog.setMaximumSize(QtCore.QSize(403, 600))
        DirProgressDialog.setModal(True)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(DirProgressDialog)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.dialogLayout = QtWidgets.QVBoxLayout()
        self.dialogLayout.setSpacing(1)
        self.dialogLayout.setObjectName("dialogLayout")
        self.groupFiles = QtWidgets.QGroupBox(DirProgressDialog)
        self.groupFiles.setObjectName("groupFiles")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupFiles)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.scrollArea = QtWidgets.QScrollArea(self.groupFiles)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 367, 333))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.scrollLayout.setContentsMargins(0, 0, 0, 0)
        self.scrollLayout.setSpacing(0)
        self.scrollLayout.setObjectName("scrollLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 270, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.scrollLayout.addItem(spacerItem)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout_2.addWidget(self.scrollArea)
        self.dialogLayout.addWidget(self.groupFiles)
        self.cancelLayout = QtWidgets.QHBoxLayout()
        self.cancelLayout.setSpacing(0)
        self.cancelLayout.setObjectName("cancelLayout")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.cancelLayout.addItem(spacerItem1)
        self.btnCancel = QtWidgets.QPushButton(DirProgressDialog)
        self.btnCancel.setObjectName("btnCancel")
        self.cancelLayout.addWidget(self.btnCancel)
        self.dialogLayout.addLayout(self.cancelLayout)
        self.verticalLayout_3.addLayout(self.dialogLayout)

        self.retranslateUi(DirProgressDialog)
        QtCore.QMetaObject.connectSlotsByName(DirProgressDialog)

    def retranslateUi(self, DirProgressDialog):
        _translate = QtCore.QCoreApplication.translate
        DirProgressDialog.setWindowTitle(_translate("DirProgressDialog", "Dialog"))
        self.groupFiles.setTitle(_translate("DirProgressDialog", "Scanning Files..."))
        self.btnCancel.setText(_translate("DirProgressDialog", "Cancel"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    DirProgressDialog = QtWidgets.QDialog()
    ui = Ui_DirProgressDialog()
    ui.setupUi(DirProgressDialog)
    DirProgressDialog.show()
    sys.exit(app.exec_())

