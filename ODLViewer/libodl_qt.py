""" Additional backend functions for ODL handling,
requiring QT for multithread handling.
"""
import os
from math import floor
from os.path import abspath, getsize, basename

from PyQt5.QtCore import QThread, pyqtSignal, QObject, pyqtSlot

from libodl import parse_line, ODLFormatException


class ThreadedLineCounter(QObject):

    counted = pyqtSignal(int, name="counted")
    finished = pyqtSignal()

    def __init__(self, file_path):
        super().__init__(parent=None)
        self.file_path = file_path
        self.lines = 0
        self.stop_requested = False

    @pyqtSlot()
    def stop(self):
        self.stop_requested = True

    @pyqtSlot()
    def start(self):
        # from http://stackoverflow.com/questions/845058/how-to-get-line-count-cheaply-in-python
        last_update = 0
        with open(self.file_path, 'rb') as f:
            buf_size = 1024 * 1024
            read_f = f.raw.read
            buf = read_f(buf_size)
            while buf:
                if self.stop_requested is True:
                    self.lines = 0
                    return

                self.lines += buf.count(b'\n')
                if self.lines > last_update + 100:
                    self.counted.emit(self.lines)
                    last_update = self.lines
                buf = read_f(buf_size)
        self.counted.emit(self.lines)
        self.finished.emit()
        return


class ThreadedFileParser(QObject):
    """ Threaded object that will parse a log, periodically reporting on progress
    """

    parsedLines = pyqtSignal(int, name="parsedLines")
    finished = pyqtSignal(name="finished")

    def __init__(self, file_path):
        super().__init__(parent=None)

        self.file_path = file_path
        self.messages = []
        self.stop_requested = False

    @pyqtSlot()
    def stop(self):
        self.stop_requested = True

    @pyqtSlot()
    def start(self):
        with open(self.file_path, 'r', encoding='utf-8') as f:
            for index, line in enumerate(f):

                if self.stop_requested is True:
                    self.messages = []
                    return

                if index == 0 and not line.startswith('['):
                    raise ODLFormatException('Not an ODL file', abspath(self.file_path))

                # if it's a new message, parse it
                if line.startswith('['):
                    try:
                        msg = parse_line(line, index, basename(self.file_path))
                        self.messages.append(msg)
                    except ODLFormatException as oe:
                        # continuation of previous message text (?)
                        msg.text += os.linesep + line
                else:
                    # continuation of previous message text
                    msg.text += os.linesep + line
                if index % 100 == 0:
                    self.parsedLines.emit(index)
        self.finished.emit()
        return


class FileCounterParser(QObject):
    """ Coordinated counting + parsing.
    Note that this does not need to be moved to its own thread,
    it already farms out long operations internally.
    """

    # tracking progress while counting lines
    linesCounted = pyqtSignal(int, name="linesCounted")
    # returning total number of lines
    totalLines = pyqtSignal(int, name="totalLines")
    # tracking progress while parsing
    linesParsed = pyqtSignal(int, name="linesParsed")
    # signaling results
    finished = pyqtSignal(name="finished")
    canceled = pyqtSignal(name="canceled")


    def __init__(self, file_path):
        super().__init__()
        self.file_path = file_path
        self.messages = []
        self.num_lines = 0
        self.stop_requested = False
        self.completed = False

        # There will be two threads: one for counting lines,
        # and one for parsing messages. The latter will be
        # created only after the former is completed.
        #
        # Each thread needs two objects: a QThread and a
        # worker that will run on it.

        self._counter_thread = QThread()
        self._counter_worker = ThreadedLineCounter(self.file_path)
        self._parser_thread = None
        self._parser_worker = None

        # we connect thread start with worker start,
        # then assign worker to thread.

        self._counter_thread.started.connect(self._counter_worker.start)
        self._counter_worker.counted.connect(self._countingLines)
        self._counter_worker.finished.connect(self._lines_counted)
        self._counter_worker.moveToThread(self._counter_thread)

        # utility to simplify downstream use
        file_size = getsize(self.file_path)  # in bytes
        self.estimated_lines = floor(file_size / (4 * 1024))  # assume 4kb per line


    # the following two slots have been wired to worker signals
    # so we can emit our own signals in our own thread/loop

    @pyqtSlot(int)
    def _countingLines(self, num_lines):
        self.linesCounted.emit(num_lines)

    @pyqtSlot(int)
    def _parsingLines(self, num_lines):
        self.linesParsed.emit(num_lines)

    @pyqtSlot()
    def start(self):
        """ Kick off execution
        """
        self._counter_thread.start()

    @pyqtSlot()
    def cancel(self):
        """ Abort execution as gracefully as possible.
        """
        if not self.completed:
            self.stop_requested = True
            if self._parser_thread is not None:
                # parsing has already started, cancel it
                self._parser_thread.requestInterruption()
                self._parser_worker.stop()
            if self._counter_thread is not None:
                if self._counter_thread.isRunning():
                    self._counter_thread.requestInterruption()
                    self._counter_worker.stop()
            self.canceled.emit()

    @pyqtSlot()
    def _lines_counted(self):
        # when we get here, the counting has completed, so we can proceed
        self.num_lines = self._counter_worker.lines
        self.totalLines.emit(self.num_lines)
        # create thread and worker for parsing
        self._parser_thread = QThread()
        self._parser_worker = ThreadedFileParser(self.file_path)
        # wire them all up and start
        self._parser_thread.started.connect(self._parser_worker.start)
        self._parser_worker.parsedLines.connect(self._parsingLines)
        self._parser_worker.finished.connect(self._done)
        self._parser_worker.moveToThread(self._parser_thread)
        try:
            self._parser_thread.start()
        except ODLFormatException as e:
            self.cancel()

    @pyqtSlot()
    def _done(self):
        # parsing has now completed
        self.messages = self._parser_worker.messages
        self._parser_worker.deleteLater()
        self._counter_worker.deleteLater()
        self._parser_thread.deleteLater()
        self._counter_thread.deleteLater()
        self.completed = True
        self.finished.emit()

