from math import floor
from os.path import abspath, basename, getsize

from PyQt5.QtCore import QThread, Qt, pyqtSlot
from PyQt5.QtWidgets import QProgressDialog

from libodl_qt import ThreadedLineCounter, ThreadedFileParser
from ovlog import OVLogWindow, _translate


class OVLogFileWindow(OVLogWindow):
    """ filebound version of OVLogstackWindow
    """
    # there are two steps to file loading:
    # first we count lines in file, so we can set up a progressbar
    # for the long parsing job; and then we actually do that parsing.
    # Each step needs 3 elements: a progress dialog, a QThread, and
    # a QObject actually doing the work.
    # The process is started in .load(), which cascades
    # depending on actual events.

    def __init__(self, file_path, parent=None):
        super().__init__(parent=parent)
        self.file_path = abspath(file_path)
        self.setWindowTitle(basename(file_path))
        self.hide()

        # set up empty references to be used later
        self.parsing_thread = None
        self._threaded_parser = None
        self.parsing_progress = None

        # set up architecture for the counting job
        self.counter_thread = QThread()     # empty qthread
        self._threaded_counter = ThreadedLineCounter(self.file_path)  # qobject doing work
        self.counter_progress = RunningThreadProgressDialog("Counting lines in file...",
                                                            self._threaded_counter,
                                                            self._threaded_counter.counted,
                                                            parent=self)  # progress dialog
        # connect start and stop signals. Note we don't start anything yet.
        self.counter_progress.canceled.connect(self._line_count_aborted)
        self.counter_thread.started.connect(self._threaded_counter.start)
        self._threaded_counter.finished.connect(self._lines_counted)

    def load(self):
        """ Kickstart the loading process
        """
        self.counter_progress.show()
        # make a rough estimate of how long the line-counting job will take
        file_size = getsize(self.file_path)  # in bytes
        estimated_lines = floor(file_size / (4 * 1024))  # assume 4kb per line
        self.counter_progress.setMaximum(estimated_lines)
        # associate object to thread and start the job
        self._threaded_counter.moveToThread(self.counter_thread)
        self.counter_thread.start()

    def _line_count_aborted(self):
        """ cleanup if counting is canceled
        """
        self._threaded_counter.stop()
        # if the signal gets here when it's too late and the parser has started,
        # stop that as well
        if self._threaded_parser:
            self._threaded_parser.stop()
            self.parsing_progress.close()
        self.close()
        if self.subWindow:
            self.subWindow.close()

    def _lines_counted(self):
        """ this will execute once lines have been counted,
        kkickstarting the actual parsing job
        """
        # again thread, object, and progress dialog
        self.parsing_thread = QThread()
        self._threaded_parser = ThreadedFileParser(self.file_path)
        self.parsing_progress = RunningThreadProgressDialog("Parsing {}...".format(basename(self.file_path)),
                                                            self._threaded_parser,
                                                            self._threaded_parser.parsedLines,
                                                            parent=self)
        # set the progressbar max value
        self.parsing_progress.setMaximum(self._threaded_counter.lines)

        # connect signals, move the object to thread, and start the job
        self.parsing_progress.canceled.connect(self._parsing_aborted)
        self.parsing_thread.started.connect(self._threaded_parser.start)
        self._threaded_parser.finished.connect(self._messages_parsed)
        self._threaded_parser.moveToThread(self.parsing_thread)
        self.parsing_thread.start()

    def _parsing_aborted(self):
        """ cleanup if parsing is canceled
        """
        self._threaded_parser.stop()
        self.close()
        if self.subWindow:
            self.subWindow.close()

    def _messages_parsed(self):
        """ this will execute once parsing has completed,
        drawing the final window
        """
        self.load_messages(self._threaded_parser.messages)
        if self.subWindow:
            self.subWindow.show()
        self.show()


class RunningThreadProgressDialog(QProgressDialog):
    """ Specialized progress dialog
    """
    def __init__(self, message, threaded_object, setValueSignal, parent=None):
        """

        :param message: Message to display
        :param threaded_object: worker QObject subclass
        :param setValueSignal: callable that will be connected to .setValue(int)
        :param parent: parent object (required for nicer popups on OSX)
        """
        super().__init__(parent=parent)
        self.threaded_object = threaded_object
        self.hide()  # we hide to make sure modality is applied (as per doc)
        self.setWindowModality(Qt.WindowModal)
        self.setLabelText(message)
        self.setCancelButtonText(_translate("LogMain", "Cancel"))
        self.setMinimum(0)
        self.setMaximum(10000000)
        self.setAutoClose(True)
        self.setAutoReset(True)
        self.setMinimumDuration(0)

        self.threaded_object.finished.connect(self._done)
        setValueSignal.connect(self.setProgressValue)  # we use an internal slot for safety

    @pyqtSlot(int)
    def setProgressValue(self, int):
        self.setValue(int)

    @pyqtSlot()
    def _done(self):
        # regardless of actual numbers, if the thread tells us it's done, it's done
        self.setValue(self.maximum())