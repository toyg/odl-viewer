# -*- coding: utf-8 -*-
import os
from os.path import basename, join
from time import sleep

from PyQt5.QtCore import pyqtSlot, QCoreApplication
from PyQt5.QtWidgets import QDialog, QMainWindow, QApplication, QFrame

from libodl_qt import FileCounterParser
from ui.dirprogressdialog import Ui_DirProgressDialog
from ui.progressframe import Ui_frameProgress

_translate = QCoreApplication.translate


class FileProgressFrame(QFrame, Ui_frameProgress):
    def __init__(self, workerThread, parent=None):
        super().__init__(parent=parent)
        self.setupUi(self)
        self.worker = workerThread
        # set initial display
        self.lFilename.setText(basename(workerThread.file_path))
        self.lStatus.setText(_translate("FileProgressFrame", "Estimating workload..."))
        self.progress.setMaximum(self.worker.estimated_lines)
        # wire signals and start
        self.worker.linesParsed.connect(self._setProgressValue)
        self.worker.totalLines.connect(self._start_parsing)
        self.worker.start()

    @pyqtSlot(int)
    def _setProgressValue(self, value):
        self.progress.setValue(value)

    @pyqtSlot(int)
    def _start_parsing(self, value):
        # Lines have been counted.
        # change status
        self.lStatus.setText(_translate("FileProgressFrame", "Parsing file..."))
        # reset progress bar
        self.progress.setValue(0)
        self.progress.setMaximum(value)
        # rewire
        self.worker.linesParsed.connect(self._setProgressValue)
        self.worker.finished.connect(self._done)

    @pyqtSlot()
    def _done(self):
        self.messages = self.worker.messages
        self.hide()

    def cancel(self):
        self.worker.cancel()
        self.lStatus.setText(_translate("FileProgressFrame", "Canceled"))
        self.hide()


class DirProgressDialog(QDialog, Ui_DirProgressDialog):
    def __init__(self, dir_path, log_window, parent=None):
        super().__init__(parent=parent)
        self.setupUi(self)
        self.log_win = log_window
        self.show()
        self.dir_path = dir_path
        self.messages = []
        self.frames = []
        self.total_jobs = 0
        self._done_jobs = 0

        self.btnCancel.clicked.connect(self._cancel)
        for dirpath, dirnames, filenames in os.walk(dir_path):
            for f_name in filenames:
                f_path = join(dir_path, f_name)
                worker = FileCounterParser(f_path)
                f_frame = FileProgressFrame(worker)
                worker.finished.connect(self._done_file)
                self.frames.append(f_frame)
                self.scrollLayout.insertWidget(self.scrollLayout.count()-2,
                                               f_frame)
                self.total_jobs += 1
                worker.start()
                # don't threadbomb
                if len(self.frames) > 0:
                    if len(self.frames) % 10 == 0:
                        sleep(3)

    @pyqtSlot()
    def _cancel(self):
        for f in self.frames:
            f.cancel()
        self.hide()
        self.log_win.hide()
        self.log_win.subWindow.close()



    @pyqtSlot()
    def _done_file(self):
        messages = self.sender().messages
        self.messages.extend(messages)
        self._done_jobs += 1
        if self._done_jobs == self.total_jobs:
            self.log_win.load_messages(self.messages)
            self.log_win.show()
            self.hide()



if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    LogMain = QMainWindow()
    ui = DirProgressDialog()
    ui.show()
    sys.exit(app.exec_())
