from os.path import expanduser, join, dirname, abspath, exists
from datetime import datetime

from PyQt5.QtCore import QSettings, Qt
from PyQt5.QtWidgets import QMainWindow, QMdiSubWindow, QFileDialog, QApplication, QDialog, QMessageBox

from libodl import parse_dir
from ovlog import OVLogWindow
from ovlogfile import OVLogFileWindow
from ovlogfiles import DirProgressDialog
from ui.folder_find_dialog import Ui_dlgFind
from ui.mainwindow import Ui_MainWindow


_ABOUTMSG_TEMPLATE = "<center style='font-weight: normal;'><b>{app} {version}</b><br>" \
                  "©️ {year} {company};<br>released under terms of General Public License v.3.<br><br>" \
                  "For info, contact <a href='mailto:{author_email}'>{author_email}</a>.<br><br>" \
                  "<small>Built with <a href='http://www.riverbanksoftware.com'>PyQt</a> and " \
                  "<a href='http://www.qt.io'>Qt</a> under terms of the GPL license.<br>" \
                  "Contains artwork by <a href='http://www.everaldo.com'>Everaldo</a> and " \
                  "<a href='http://www.iconeden.com/'>IconEden</a>, used under permission.</center>"

_SETTING_LAST_OPEN_DIR = 'last_open_dir'

class OVMainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__()
        self.setupUi(self)
        # generic and display
        self.settings = QSettings()
        self.setWindowTitle(QApplication.instance().applicationDisplayName())
        self.mainToolBar.setOrientation(Qt.Vertical)
        self.addToolBar(Qt.LeftToolBarArea, self.mainToolBar)
        # sub-dialog
        self.findDialog = OVFindDialog(self)
        self.findDialog.accepted.connect(self.find_in_dir)
        self.findDialog.hide()
        # connect
        self.actionOpen_Log.triggered.connect(self.open_file)
        self.actionOpen_Directory.triggered.connect(self.open_directory)
        self.actionFind_in_Directory.triggered.connect(self.findDialog.show)
        self.actionAbout.triggered.connect(self.about)
        # icons
        self.actionOpen_Log.setIcon(QApplication.instance().icons['open'])
        self.actionOpen_Directory.setIcon(QApplication.instance().icons['open_folder'])
        self.actionFind_in_Directory.setIcon(QApplication.instance().icons['search_folder'])
        self.actionAbout.setIcon(QApplication.instance().icons['about'])

        self.show()

    def about(self):
        """ Simple "about" message
        """
        qApp = QApplication.instance()
        msg = _ABOUTMSG_TEMPLATE.format(
            app=qApp.applicationDisplayName(),
            version=qApp.applicationVersion(),
            year="2015-" + str(datetime.now().year),
            company=qApp.organizationName(),
            author_email=self.settings.value('email_contact')
        )
        QMessageBox.about(None, "About " + qApp.applicationDisplayName(), msg)

    def _create_subwindow(self, widget):
        """ Utility function to create subwindow in MDI area
        :param widget: a qdialog or qmainwindow to use in subwindow
        """
        sub = QMdiSubWindow()
        sub.setWidget(widget)
        widget.subWindow = sub
        self.mdiArea.addSubWindow(sub)
        sub.showMaximized()


    def open_file(self):
        """ Open a log file and display it in a subwindow
        """
        last_dir = self.settings.value(_SETTING_LAST_OPEN_DIR,
                                       join(expanduser('~'), 'Desktop'),
                                       str)
        log_paths, extra = QFileDialog.getOpenFileNames(self, "Choose logs to open", last_dir)
        if log_paths:
            ldir = abspath(dirname(log_paths[0]))
            self.settings.setValue(_SETTING_LAST_OPEN_DIR, ldir)
            for file_path in log_paths:



                log_win = OVLogFileWindow(file_path, parent=self)
                self._create_subwindow(log_win)
                log_win.load()
            # if opening multiple windows, tilethem
            if len(log_paths) > 1:
                self.mdiArea.tileSubWindows()

    def open_directory(self):
        """ Parse all files in a directory
        and display resulting messages in a subwindow
        """
        last_dir = self.settings.value(_SETTING_LAST_OPEN_DIR,
                                       join(expanduser('~'), 'Desktop'),
                                       str)
        dir_root = QFileDialog.getExistingDirectory(self, "Choose directory to scan",
                                                           last_dir)
        if dir_root:
            self.settings.setValue(_SETTING_LAST_OPEN_DIR, dir_root)
            log_win = OVLogWindow(parent=self)
            self._create_subwindow(log_win)
            log_win.setWindowTitle(abspath(dir_root))
            log_win.hide()
            dir_dlg = DirProgressDialog(dir_root, log_win, parent=self)


    def find_in_dir(self, *args, **kwargs):
        """ Search for a string in an entire directory
        """
        dir_root = self.findDialog.leFolder.text()
        key = self.findDialog.leFind.text()
        if not exists(dir_root):
            QMessageBox.critical(self, "Wrong folder!", "The specified directory does not exist.")
            self.findDialog.show()
            return False
        if not key:
            QMessageBox.critical(self, "No key!", "Please specify what to search for.")
            self.findDialog.show()
            return False
        try:
            all_messages = parse_dir(dir_root)
            # extremely naive and inefficient search algo follows. Suggestions welcome.
            matching = []
            for m in all_messages:
                for f in m.fields:
                    if f not in ['logfile', 'timestamp']:
                        if key in str(getattr(m, f)):
                            matching.append(m)
            if not matching:
                QMessageBox.information(self, "No matches!", "Sorry, the specified text could not be found.")
                return False
            log_win = OVLogWindow(messages=matching, parent=self)
            self._create_subwindow(log_win)
        except Exception as e:
            QMessageBox.Critical("Error trying to search in folder {folder}: {err}".format(folder=dir_root,
                                                                                           err=str(e)))


class OVFindDialog(QDialog, Ui_dlgFind):
    """ Simple dialog for "Find in Folder"
    """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.tlbFolder.clicked.connect(self._select_folder)

    def _select_folder(self, *args, **kwargs):
        last_dir = QSettings().value(_SETTING_LAST_OPEN_DIR,
                                       join(expanduser('~'), 'Desktop'),
                                       str)
        dir_root = QFileDialog.getExistingDirectory(self, "Choose directory to scan",
                                                           last_dir)
        if dir_root:
            self.leFolder.setText(dir_root)