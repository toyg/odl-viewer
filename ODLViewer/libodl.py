import logging
from operator import attrgetter
import os
import re
from os.path import join, abspath, basename
from datetime import datetime


logging.basicConfig(level=os.environ.get('LOGLEVEL', logging.WARNING))

_RE_SEP = re.compile('\]\s+\[')


def timestamp_to_pydatetime(timestamp_string):
    # There are two formats:
    # 2016-10-01T23:22:09
    # and
    # 2016-01-10T23:22:09-00:00
    # we basically just drop the timezone at the moment,
    # because the format doesn't match what python can recognize oob
    # and tbh nobody cares in this context

    return datetime.strptime(timestamp_string[0:18], '%Y-%m-%dT%H:%M:%S')


class LogMessage:
    """ Represents a single error message.
    """

    REQUIRED_FIELDS = ['log_line',
                       'timestamp',
                       'type',
                       'id',
                       'text',
                       'module_id',
                       'component_id',
                       'ecid',
                       'filename']

    # current implementation is basically a generic "bag of holding"...
    def __init__(self, *args, **kwargs):
        for key, value in kwargs.items():
            self.__dict__[key] = value

    def __set__(self, instance, value):
        self.__dict__[instance] = value

    def __getattr__(self, item):
        if not item in self.__dict__:
            return ''
        else:
            return self.__dict__[item]

    @property
    def fields(self):
        # compulsory fields
        fields = self.REQUIRED_FIELDS + self.optional_fields
        return fields

    @property
    def optional_fields(self):
        return [key for key in vars(self).keys() if key not in self.REQUIRED_FIELDS]


class ODLFormatException(Exception):
    def __init__(self, message, source, source_type='file'):
        """ Something went wrong parsing text or file
        :param message: string message
        :param source: the offending file or text
        :param source_type: 'file' or 'text'
        """
        super().__init__(message)
        self.message = message
        self.type = source_type
        self.source = source


def parse_line(line, line_index=0, file_name=None):
    """ Parse a log line and return LogMessage on success
    :param line: string to parse
    :param line_index: int indicating position in log
    :return: LogMessage
    """
    if line.startswith('['):
        tokens = _RE_SEP.split(line, maxsplit=20)
        tokens[0] = tokens[0][1:]  # strip first [
        # manually split the last one
        lasts = tokens[-1].split('] ', 1)
        if len(lasts) > 1:
            tokens[-1] = lasts[0]
            tokens.append(lasts[1])
        msg = LogMessage()
        msg.filename=file_name
        msg.timestamp = timestamp_to_pydatetime(tokens[0])
        msg.module_id = tokens[1]
        msg.type = tokens[2]
        msg.id = tokens[3]
        msg.component_id = tokens[4]
        msg.text = tokens[-1]
        msg.log_line = line_index
        prev_field = 'component_id'

        for token in tokens[5:-1]:
            try:
                key, value = token.split(': ', 1)
                setattr(msg, key.strip(), value)
                prev_field = key
            except ValueError as ve:
                # there might be situations where we have
                # the form '] [ something something ] ['
                # in that case we should append it to the
                # previous field, since it might be part of
                # a non-ODL message of some sort
                prev_field_text = getattr(msg, prev_field)
                setattr(msg, prev_field, prev_field_text + '] [' + token + '] [')
        return msg
    else:
        raise ODLFormatException("Not an ODL line", line, 'text')


def parse_dir(dir_path):
    """ Parse an entire directory, recursively, and return all parsed ODL files found.

    :param dir_path: string path to directory
    :return: list of tuples (absolute_file_path, [list of LogMessage])
    """
    result = []
    for dirpath, subdirs, files in os.walk(dir_path):
        for file_name in files:
            relpath = join(dir_path, file_name)
            f_path = abspath(relpath)
            try:
                messages, next_line = parse_file(f_path)
                for m in messages:
                    m.logfile = relpath
                result.extend(messages)
            except ODLFormatException as e:
                logging.debug(e.file_path + ': ' + e.message)
                continue
            except OSError as oe:
                logging.warning("Could not open {}".format(f_path))
                continue

    return sorted(result, key=attrgetter('timestamp'))


def parse_file(file_path, start_line=0, max_messages=0):
    """ Parse a file and return LogMessage instances.

    :param file_path: string path to ODL file
    :param start_line: int of line to start parsing from
    :param max_messages: int max number of LogMessage to return
    :return: tuple (list of LogMessage, int next line in file)
    """
    messages = []
    with open(file_path, 'r', encoding='utf-8') as file_object:
        msg = None
        for index, line in enumerate(file_object):
            if index == 0 and not line.startswith('['):
                raise ODLFormatException('Not an ODL file', abspath(file_path))

            # if we got enough messages, stop
            if max_messages > 0:
                if len(messages) >= max_messages:
                    return messages, index
            # skip all lines before start_line
            if index < start_line:
                continue

            # if it's a new message, parse it
            if line.startswith('['):
                try:
                    msg = parse_line(line, index)
                    messages.append(msg)
                except ODLFormatException as oe:
                    # continuation of previous message text (?)
                    msg.text += os.linesep + line
                    logging.warning("This line is funny: " + line)
            else:
                # continuation of previous message text
                msg.text += os.linesep + line
    return messages, None


if __name__ == '__main__':
    f = '/Users/toyg/Dev/ODLViewer/test_data/aif-CalcManager.log'
    print(parse_file(f))

