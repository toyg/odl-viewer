from PyQt5.QtCore import QAbstractTableModel, Qt, QVariant, QDateTime, QSortFilterProxyModel

from PyQt5.QtGui import QColor, QBrush
from libodl import LogMessage

# QT models

class PropertyModel(QAbstractTableModel):
    """ Used by the "Properties" tableview displayed on OVLogWindow for
    extra properties.
    """
    # fields that are displayed elsewhere
    DEFAULT_HIDDEN = ['log_line',
                  'timestamp',
                  'type',
                  'id',
                  'text',
                  'module_id',
                  'component_id',
                  'ecid']

    def __init__(self, logfile_message, parent=None, *args):
        super().__init__(parent)
        self.message = logfile_message
        self.fields = [f for f in self.message.fields if f not in self.DEFAULT_HIDDEN]

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self.fields)

    def columnCount(self, parent=None, *args, **kwargs):
        return 2

    def headerData(self, p_int, Qt_Orientation, role=None):
        header_map = ['Property', 'Value']
        if Qt_Orientation == Qt.Horizontal and role in (Qt.DisplayRole, Qt.ToolTipRole):
            return header_map[p_int]

    def data(self, index, role=None):
        if not index.isValid():
            return QVariant()
        elif role not in (Qt.DisplayRole, Qt.EditRole, Qt.ToolTipRole):
            return QVariant()

        row_field = self.fields[index.row()]
        if index.column() == 0:
            return row_field
        elif index.column() == 1:
            return getattr(self.message, row_field)


class LogstackModel(QAbstractTableModel):
    """ Model representing a stack of messages. Returns LogMessage objects in UserRole.
    """
    def __init__(self, messages, parent=None, *args):
        super().__init__(parent)
        self.messages = messages
        # messages can be of various types, with different fields,
        # so find out all the optional fields available
        all_extra_fields = set()
        for m in self.messages:
            all_extra_fields.update(m.optional_fields)
        self.fields = LogMessage.REQUIRED_FIELDS + list(all_extra_fields)


    def rowCount(self, parent=None, *args, **kwargs):
        return len(self.messages)

    def columnCount(self, parent=None, *args, **kwargs):
        return len(self.fields)

    def headerData(self, p_int, Qt_Orientation, role=None):
        if Qt_Orientation == Qt.Horizontal and role in (Qt.DisplayRole, Qt.ToolTipRole):

            field_name = self.fields[p_int]
            if field_name.lower() in ['id', 'ecid', 'pid']:
                field_name = field_name.upper()
            elif field_name == 'userId':
                field_name = "User ID"
            elif field_name == 'module_id':
                field_name = 'Module'
            elif field_name == 'component_id':
                field_name = "Component"
            elif field_name == 'tid':
                field_name = 'Thread ID'
            elif field_name == 'timestamp':
                field_name = 'Time'
            elif field_name == 'log_line':
                field_name = 'Log Line'
            else:
                field_name = field_name.capitalize()
            return QVariant(field_name)

    def data(self, index, role=None):
        if not index.isValid():
            return QVariant()
        elif role == Qt.UserRole:
            return self.messages[index.row()]
        elif role not in (Qt.DisplayRole, Qt.EditRole, Qt.ToolTipRole, Qt.BackgroundColorRole):
            return QVariant()

        field = self.fields[index.column()]
        msg = self.messages[index.row()]

        if role == Qt.BackgroundColorRole:
            map = {'ERROR': QColor(236, 163, 163),
               'WARN': QColor(251, 210, 147),
               #'NOTIFICATION': QColor(255,255,255),
               #'INFO': QColor(255,255,255),
               #'DEBUG': QColor(211,208,205),
               #'TRACE': QColor(211,208,205),
               }
            for key in map:
                if msg.type.startswith(key):
                    return QBrush(map[key])
            return QBrush()

        if field == 'timestamp':
            return QDateTime(msg.timestamp)

        return QVariant(getattr(msg, field))


class LogfileModelProxy(QSortFilterProxyModel):
    """ Proxy for LogstackModel to enable arbitrary sorting and filtering.
    The default filtering key is always kept on logging-level,
    then further criteria are checked in filterAcceptsRow()
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.minDate = None
        self.maxDate = None
        self.filters = {}

    @property
    def fields(self):
        return self.sourceModel().fields

    def filterAcceptsRow(self, p_int, parentIndex):
        # first, let's see if it's acceptable by default constraints (i.e. logging-level toggles)
        acceptable = super().filterAcceptsRow(p_int, parentIndex)
        if not acceptable:
            return False

        # then check if we have to filter it for other reasons
        #model = self.sourceModel()
        model = self.sourceModel()

        # -- special filter for dates
        if self.minDate or self.maxDate:
            index = model.index(p_int, model.fields.index('timestamp'), parentIndex)
            qdate = model.data(index, Qt.DisplayRole)
            if self.minDate and qdate < self.minDate:
                return False
            if self.maxDate and qdate > self.maxDate:
                return False

        # -- generic string search for any other field if set
        if self.filters:
            results = []
            for param, value in self.filters.items():
                result = False
                index = model.index(p_int, model.fields.index(param), parentIndex)
                data = model.data(index, Qt.DisplayRole).value()
                if data != '' and data is not None:
                    result = self.filters[param].lower() in data.lower()
                results.append(result)
            if results and not all(results):
                return False

        # still here? matches all prerequisites, go on.
        return True

