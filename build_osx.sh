#!/usr/bin/env bash
rm -rf build dist
export PYINSTALLER_CONFIG_DIR=pyinstaller_config_osx
pyinstaller --clean ODLViewer_osx.spec

#if you want to recreate the .spec (DON'T! or at least make a backup),
# you should run:
# pyinstaller \
#    --osx-bundle-identifier com.pythonaro.odlviewer \
#    -i odlviewer/icons/osx.icns  \
#    --clean -F -w   \
#    -p odlviewer:odlviewer/ui:env/lib/python3.5:env/lib/python3.5/site-packages \
#    odlviewer/ODLViewer.py
#
