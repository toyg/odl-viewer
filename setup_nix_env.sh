#!/usr/bin/env bash

# You should have python 3.4 or 3.5 installed already
python3 -m venv env
source env/bin/activate
pip -r requirements.txt