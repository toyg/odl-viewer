powershell -command "$clnt = new-object System.Net.WebClient; $clnt.DownloadFile(\"https://www.python.org/ftp/python/3.5.1/python-3.5.1.amd64.EXE\", \"C:/PYTHON351.AMD64.EXE\")"
C:\python351.amd64.exe /passive InstallAllUsers=1 TargetDir=C:\Python35 AssociateFiles=0 CompileAll=1 Include_dev=0 Include_tcltk=0 Include_test=0
c:\python35\python.exe -m venv env
call env\scripts\activate.bat
pip install -r requirements.txt
del c:\python351.amd64.exe
