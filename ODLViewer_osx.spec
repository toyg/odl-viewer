# -*- mode: python -*-

block_cipher = None


a = Analysis(['odlviewer/ODLViewer.py'],
             pathex=['odlviewer', 'odlviewer/ui', 'env/lib/python3.5', 'env/lib/python3.5/site-packages', '.'],
             binaries=[],
             datas=[
                 ('odlviewer/icons', 'icons'),
                 ('README.md', 'README.md')
             ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='ODLViewer',
          debug=False,
          strip=False,
          upx=True,
          console=False , icon='odlviewer/icons/osx.icns')
app = BUNDLE(exe,
             name='ODLViewer.app',
             icon='odlviewer/icons/osx.icns',
             bundle_identifier='com.pythonaro.odlviewer',
             info_plist={'NSHighResolutionCapable': 'True'},
             )
